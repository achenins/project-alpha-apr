from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def show_list(request):
    project_data = Project.objects.filter(owner=request.user)
    context = {
        "project_list": project_data,
    }
    return render(request, "projects/project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm
    context = {"form": form}
    return render(request, "projects/create.html", context)
