from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.models import Project
from tasks.forms import TaskForm


@login_required
def show_project(request, id):
    project_data = get_object_or_404(Project, id=id)
    task_data = Task.objects.filter(assignee=request.user)
    context = {
        "project_object": project_data,
        "task_list": task_data,
    }
    return render(request, "tasks/task.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    task_data = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": task_data,
    }
    return render(request, "tasks/mine.html", context)
